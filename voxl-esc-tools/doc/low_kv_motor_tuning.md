# Hardware Pre-requisites
- M0134-6: for 6S applications, install provided external capacitor to the ESC at the battery input pads!


- For best results (easy to view plots), use USB to serial adapter to connect to a linux PC
- Running voxl-esc tools on VOXL2 is fine, but the plots will be saved to disk as opposed to shown right away in browser
   - make sure to stop `px4` before using `voxl-esc` tools using the following command: `systemctl stop voxl-px4`

# Procedure Outline
- ideally, use the latest version of voxl-esc from gitlab dev branch
- query the ESCs to make sure they can be detected (`./voxl-esc-scan.py`)
- install the latest firmware (`./voxl-esc-upload-firmware-all.sh`)
   - firmware version 39 RC11 or later must be used for low-kv motor testing
   - firmware tested in this example : `modalai_esc_firmware_m0134_6_v0_39_RC11_17d64675.bin`
- update and install initial params (`./voxl-esc-upload-params.py --params-file ../voxl-esc-params/low_kv/mn4006_m0134_6_low_kv.xml`)
- perform step tests to confirm stable performance in open loop (power commands)
- perform RPM curve calibration and update params
- perform RPM step tests to confirm stable performance

# Tuning Example
- motor MN4006-23 380kV, 18N24P winding configuration
   - 12 pole pairs (24/2)
- 15-inch propeller MS1503
- 6S battery voltage

## Parameter Prep
- start with a sample param file `mn4006_m0134_6_low_kv.xml`

## Basic Battery and Motor Information
- `vbat_nominal_mv: 22200`
- `num_cycles_per_rev: 12`
- `motor_kv : 380` (set actual motor kV value. important for sinusoidal spin-up)

## Reset rpm calibration, kp, ki to zero (will disable rpm control for now)
- `pwm_vs_rpm_curve_a0: 0`
- `pwm_vs_rpm_curve_a1: 0`
- `pwm_vs_rpm_curve_a2: 0`
- `kp: 0`
- `ki: 0`

## Spin-up parameters. For low-kv large motors it is better to use sinusoidal spinup
- `spinup_type: 1`
- `spinup_power: 150`
- `latch_power: 120`
- `spinup_power_ramp: 8` (unless foldable props may be set to lower value, perhaps down to 1)
- `spinup_rpm_target: 1000` (TBD depending on min rpm, or what is desired)
- `spinup_time_ms: 1000` (1000ms is a good start. shortening spin-up time for large props will require more spin-up power and could cause spin-up de-sync)
- `spinup_bemf_comp: 1`, otherwise will most likely de-sync during spin-up
- `protection_stall_check_rpm: 500` (usually set to about 0.5 of the min_rpm, but need to measure min_rpm first)

## Special low-kv tuning
- `timing_advance: 60`   # advance commutation to allow more demagnetization time
- `sense_advance: 20`    # delay back-emf sensing to allow more demagnetization time
- `demag_timing: 1`      # special tweak to help with long demagnetization times

## Upload the initial params file
- `./voxl-esc-upload-params.py --params-file ../voxl-esc-params/low_kv/mn4006_m0134_6_low_kv.xml`


## Perform Open Loop Ramp and Step Tests
- make sure the vehicle is securely attached to a test bench
- use single motor for initial tuning (pick appropriate ESC id)
- propeller is mounted to the motor that is being tested
- power ramp test from 10 to 100%
- short test to avoid possitiblity of damage in case something goes wrong
- set VOXL2 to performance mode to reduce the latency in UART communication
   - `voxl-set-cpu-mode perf`

### Power Ramp Test
```
./voxl-esc-spin.py --id 3 --power 100 --ramp-time 3.0 --timeout 3.5 --enable-plot 1 --cmd-rate 250
```
- result should be smooth curve, no spikes or glitches except for a little spike between transition from sinusoidal spin up to normal operation (around t=1s)
   - the spin-up duration is specified in the ESC params, we set to 1000ms (1s) above
- maximum RPM can also be noted at this point when power reaches 100% (RPM is printed in terminal)
- see the image below for an example plot (plot is also saved as `voxl_esc_spin_results.html` by the `voxl-esc-spin` test)
![Power Ramp Test](img/power_ramp_test_6s.png)

### Power Step Tests
- Perform a sequence of short step tests with increasing `step-amplitude` from 30 to 80%
- Maximum power applied in the test is equal to value of `--power ` + `--step-amplitude`
- the step is delayed (here by 1.5s) to allow the sinusoidal spin-up to complete
```
./voxl-esc-spin-step.py --id 3 --power 20 --step-delay 1.5 --step-frequency 2 --timeout 3 --enable-plot 1 --cmd-rate 250 --step-amplitude 30
./voxl-esc-spin-step.py --id 3 --power 20 --step-delay 1.5 --step-frequency 2 --timeout 3 --enable-plot 1 --cmd-rate 250 --step-amplitude 50
./voxl-esc-spin-step.py --id 3 --power 20 --step-delay 1.5 --step-frequency 2 --timeout 3 --enable-plot 1 --cmd-rate 250 --step-amplitude 70
./voxl-esc-spin-step.py --id 3 --power 20 --step-delay 1.5 --step-frequency 2 --timeout 3 --enable-plot 1 --cmd-rate 250 --step-amplitude 80
```
- Results:
   - Step 20-70
![Power Step Test 20-70](img/step_test_20_70_6s.png)
   - Step 20-90
![Power Step Test 20-90](img/step_test_20_90_6s.png)
   - Step 20-100
![Power Step Test 20-100](img/step_test_20_100_6s.png)

### What is a "de-sync" event and why does it occur
- A de-sync event occurs when the ESC is no longer in sync with the motor, meaning the ESC loses track of motor's actual phase and speed
- One common reason for de-sync is long demagnetization time of the motor windings
   - low-kv motors (< 500kV) often have high inductance, which means the current keeps flowing through the inactive coils long after the ESC commutation
   - when large current flows through the motor (often during suddent transitions from low to high rpm), the magnetization time can be so long that it prevents the ESC from correctly detecting zero crossings of back-emf
   - the solution is often to trigger phase commutation earlier (increase commutation advance) in order to allow more time for windings to "unload"
   - higher commutation advance may result in slight decrease in motor efficiency (up to 5% or more depending on many factors). The difference in efficiency can be measured by spinning at a constant rpm (hover rpm) while testing different values of commutation advance. The de-sync can usually be avoided by slowly ramping up to the desired rpm value for this test.
   - currently the ESC firmware supports a constant commutation advance but automatic advance feature is in the works

### What does a "de-sync" event look like
- In order to reproduce a de-sync condition, we set the following params to zero:
   - `timing_advance: 0`
   - `sense_advance: 0` 
- test command:
```
./voxl-esc-spin-step.py --id 3 --power 20 --step-delay 1.5 --step-frequency 2 --timeout 3 --enable-plot 1 --cmd-rate 250 --step-amplitude 50
```
- observations:
   - motor makes high-pitched squeeling sound, does not actually accelerate
   - RPM is mistakenly reported at 10K rpm, ESC lost sync with motor
   - reported RPM jumps instantly instead of gradually ramping up, as a motor would normally behave
   - low current is reported due to incorrect motor timing, although current can be much higher if motor actually stalls
![Power Step Test 20-70 Desync](img/step_test_20_70_6s_desync.png)

- Another de-sync example.
   - adjust params to better handle long demag times (but not sufficiently)
   - `timing_advance: 25`
   - `sense_advance: 20` 
   - test command is more aggressive:
```
./voxl-esc-spin-step.py --id 3 --power 20 --step-delay 1.5 --step-frequency 2 --timeout 3 --enable-plot 1 --cmd-rate 250 --step-amplitude 70
```
- result
   - de-sync happens a little later during the step, does not happen at all on the third transition
   - this means the params are close to working properly, but still need higher commutation advance
![Power Step Test 20-90 Desync](img/step_test_20_90_6s_desync.png)

# Rpm Controller Tuning

## How to find minimum and maximum RPM
- minimum and maximum RPMs are a function of battery voltage (the higher the voltage, the higher the RPM for a given power %)
- it is suggested to measure those values either at nominal battery voltage (22.2-23.0V for 6S)
- a better result would be to measure `minimum RPM` at `maximum voltage` and `maximum RPM` at `minimum voltage`.
   - Then you can always be sure the min and max rpm will be achieved regardless of battery charge level
- find minimum rpm:
./voxl-esc-spin.py --id <motor id> --power 10

- find maximum rpm (ramp up to avoid unnecessary jump and time out to avoid overheating)
./voxl-esc-spin.py --id <motor id> --power 100 --ramp-time 3.0 --timeout 3.5


## Calibrate the Feed Forward curve for RPM controller
- see `calibration.md`
- run the calibration procedure and update `pwm_vs_rpm_curve_a0`, `pwm_vs_rpm_curve_a1`, `pwm_vs_rpm_curve_a2` params
- leave `kp` and `ki` at zero or set to mild gains of `kp=50` and `ki=10` to avoid oscillations
- set `max_rpm_delta` to number that exceeds your maximum rpm to remove limiting in RPM controller
   - this parameter can be used to reduce / slow down the transition of large RPM changes


## RPM Step Tests
- after uploading the params with updated RPM parameters, we expect the ESC to accurately track RPM commands
```
./voxl-esc-spin-step.py --id 3 --rpm 2000 --step-delay 1.5  --step-frequency 1 --timeout 3.0 --enable-plot 1 --cmd-rate 250 --step-amplitude 2000
```
![RPM Step Test 2000-4000](img/step_test_2000_4000_6s.png)

```
./voxl-esc-spin-step.py --id 3 --rpm 2000 --step-delay 1.5  --step-frequency 1 --timeout 3.0 --enable-plot 1 --cmd-rate 250 --step-amplitude 3000
```
![RPM Step Test 2000-5000](img/step_test_2000_5000_6s.png)
```
./voxl-esc-spin-step.py --id 3 --rpm 2000 --step-delay 1.5  --step-frequency 1 --timeout 3.0 --enable-plot 1 --cmd-rate 250 --step-amplitude 4000
```
- in this case the power output saturated at 100% as the motor was not able to reach 6000rpm (due to low kV)
![RPM Step Test 2000-6000](img/step_test_2000_6000_6s.png)


## Additional Parameters
- protection_current_soft_limit_a can be used to limit the maximum current, which can help prevent de-sync's but will also slow down large motor speed transitions

## Increasing Responsiveness of RPM Controller
- TODO


## Other Considerations
- keep in mind that during testing the motor will heat up, winding resistance will increase, so current spikes and likelihood of de-sync's will decrease

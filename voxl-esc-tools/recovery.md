# ESC Software Recovery
This procedure describes how to recover the ESC if Firmware and Params update fails and one or more ESCs cannot be detected


## Procedure
- get the latest `voxl-esc` tools on your voxl2. You can clone directly to voxl2 or to your PC and copy to voxl2 if your voxl2 does not have internet access
```
git clone https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-esc.git -b dev
```
- make sure PX4 is not running because it will conflict with `voxl-esc` tools
```
systemctl stop voxl-px4
```
- make sure you can scan / detect ESCs . Note that the scan script may find at least one ESC running at 57600 baud rate, which would mean the param install failed (57600 is the fall back baud rate). 
This is ok. The scanning tool has recently been updated to keep searching other baud rates if all four ESCs are not detected.
```
cd voxl-esc/voxl-esc-tools
./voxl-esc-scan.py
```
- install the params based on your motor / propeller configuration or any custom ESC param that you created. This script should find all the ESCs even if they are running at different baud rates
```
./voxl-esc-upload-params.py --params-file <path_to_esc_params_file.xml>
```
- verify params - this command will detect ESCs and check if all the params are now the same
```
./voxl-esc-verify-params.py
```
- install latest firmware (this script will auto detect the board and pick the correct firmware). Please note that if the ESCs are detected at different baud rates, that needs to be fixed with the correct params (command above). This script will not automatically find the ESCs running at different baud rates (we will fix this soon).
```
./voxl-esc-upload-firmware-all.sh
```
- after recovery and testing has been completed, PX4 can be re-enabled again
```
systemctl start voxl-px4
```


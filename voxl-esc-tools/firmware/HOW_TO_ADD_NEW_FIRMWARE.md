# How to Add New ESC Firmware to the voxl-esc package

It's NOT sufficient to simple place new binaries in this directory. These firmware binaries are distributed as part of a deb package that runs on VOXL with accompanying scripts and tools to automatically upload them.

### Step 1

Place new file in the firmware directory

### Step 2

Ensure the file names all have the new git commit hash at the end.

### Step 3

Ensure there is only one of each board firmware otherwise voxl-esc script will fail. Calling ls in the firmware dir should look something like this:

```
$ cd voxl-esc-tools/firmware/
~/git/voxl-esc/voxl-esc-tools/firmware$ ls
HOW_TO_ADD_NEW_FIRMWARE.md                       modalai_esc_firmware_m0129_3_v0_38_83faccfa.bin
modalai_esc_firmware_m0049_1_v0_38_83faccfa.bin  modalai_esc_firmware_m0134_1_v0_38_83faccfa.bin
modalai_esc_firmware_m0117_1_v0_38_83faccfa.bin  modalai_esc_firmware_m0134_3_v0_38_83faccfa.bin
modalai_esc_firmware_m0117_3_v0_38_83faccfa.bin
```

#!/usr/bin/python3

from crc import crc16
import numpy as np
import serial
import time
import struct
import argparse
import string

from voxl_esc_setup_paths import *
voxl_esc_setup_paths()
from libesc import *
from esc_scanner import EscScanner


ESC_PACKET_HEADER                  = 0xAF
ESC_PACKET_TYPE_GPIO_CMD           = 15



parser = argparse.ArgumentParser(description='ESC GPIO Test Script')
parser.add_argument('--device',         required=False, default=None)
parser.add_argument('--baud-rate',      required=False, default=None)
parser.add_argument('--id',             required=False,  default=0)
parser.add_argument('--val',   type=int,required=False,  default=0)

args = parser.parse_args()

devpath  = args.device
baudrate = args.baud_rate
esc_id   = args.id
val      = args.val

if val != 1:
    val = 0

def wrap_data(packet_type, data):
    data_length = len(data)

    packet_length = data_length + 5 # add bytes for header(1), length(1), type(1), crc(2) 

    packet = [ESC_PACKET_HEADER,
              packet_length,
              packet_type]

    if data_length > 0:
        packet.extend(data)

    crc_val = crc16(packet[1:]) #do not include header byte

    packet = np.uint8(packet)
    packet = np.append(packet, np.frombuffer(crc_val, dtype=np.uint8))

    return packet


class SerialPort:
    dev       = None
    dev_path  = ''
    baud_rate = 115200

    def __init__(self, device_path, baud_rate):
        self.dev_path  = device_path
        self.baud_rate = baud_rate

    def open(self):
        self.dev              = serial.Serial()
        self.dev.port         = self.dev_path
        self.dev.baudrate     = self.baud_rate
        self.dev.parity       = serial.PARITY_NONE
        self.dev.bytesize     = serial.EIGHTBITS
        self.dev.stopbits     = serial.STOPBITS_ONE
        self.dev.timeout      = 0.025
        self.dev.writeTimeout = 0.025

        self.dev.open()
        self.dev.flush()

    def close(self):
        self.dev.flush()
        self.dev.close()

    def flush(self):
        self.dev.flushInput()
        self.dev.flush()

    def write(self, data):
        self.dev.write(data)

    def read(self):
        data_array=''
        if self.dev.inWaiting() > 0:
            data_array = self.dev.read(self.dev.inWaiting())

        return data_array


# quick scan for ESCs to detect the port
scanner = EscScanner()
(devpath, baudrate) = scanner.scan(devpath, baudrate)

if devpath is not None and baudrate is not None:
    print('INFO: ESC(s) detected on port: ' + devpath + ', baud rate: ' + str(baudrate))
else:
    print('ERROR: No ESC(s) detected, exiting.')
    sys.exit(1)


if 'slpi' in devpath:
    from voxl_serial import VoxlSerialPort
    port = VoxlSerialPort()
    port.port = devpath
    port.baudrate = baudrate
    port.open()
else:
    port = SerialPort(devpath,baudrate);
    port.open()

time.sleep(0.1);

req_data    = np.zeros(5,dtype=np.uint8)
req_data[0] = esc_id  # esc id
req_data[1] = 80      # 01010000 : pin F0 : Controls AUX regulator on M0138 ESC ID0, unsupported on other ESCs
req_data[2] = 0       # 0: output, 1: input
req_data[3] = val     # cmd LSB
req_data[4] = 0       # cmd MSB

packet = wrap_data(ESC_PACKET_TYPE_GPIO_CMD,req_data)
print('sending packet:', packet)

port.write(packet)
time.sleep(0.1)  #sleep to allow the command to go out before port is closed

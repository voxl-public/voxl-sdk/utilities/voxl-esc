#!/usr/bin/python3

import plotly.graph_objects as go
from plotly.subplots import make_subplots
import plotly.express as px

import numpy as np
import pandas as pd
import csv
import glob
import os

# Packet format for packet_type = 128
# Packet Type | Timestamp | ESC ID | Actual Power (0-100) | Desired RPM = N/A | Actual Rpm | Voltage | Current | ESC Temperature | N/A | N/A | N/A

fig = make_subplots(rows=5, cols=1, start_cell="top-left") #shared_xaxes=True


def process_file(filename, log_cntr):
      time_log    = [[],[],[],[]];
      rpm_log     = [[],[],[],[]];
      volt_log    = [[],[],[],[]];
      curr_log    = [[],[],[],[]];
      cmd_log     = [[],[],[],[]];
      temp_log    = [[],[],[],[]];

      time_offset = [0,0,0,0]

      time_scale  = 0.001
      power_scale = 1.0 #1.0 for packet 128, 0.1 for packet 130
      board_current = 0.0

      with open(filename, 'r') as x:
            reader = csv.reader(x, delimiter=' ')
            for row in reader:
                  while ('' in row):
                        row.remove('')

                  data     = np.array(row).astype(np.float64)

                  if data[0] == 132:
                        board_current = data[4]
                  
                  if data[0] != 128:
                        continue

                  esc_id = int(data[2])
                  
                  if time_offset[esc_id]==0 :
                        time_offset[esc_id] = data[1] * time_scale

                  time_log[esc_id].append(data[1] * time_scale)
                  cmd_log[esc_id].append(data[3] * power_scale)
                  rpm_log[esc_id].append(data[5])
                  volt_log[esc_id].append(data[6])
                  #curr_log[esc_id].append(data[7])
                  curr_log[esc_id].append(board_current)
                  temp_log[esc_id].append(data[8])
                  
      
      #colors = px.colors.qualitative.Plotly
      colors = px.colors.qualitative.Light24
      #colors = px.colors.qualitative.Dark24
      #colors = px.colors.qualitative.Pastel
      colors.remove(colors[3])
      ncolors = len(colors)
      cstr=colors[log_cntr%ncolors]

      for idx in range(4):
            ts = [x - time_offset[idx] for x in time_log[idx]]

            fig.add_trace(go.Scatter(x=ts, y=rpm_log[idx],    name='[%d] RPM #%d'                 % (log_cntr,idx), marker_color=cstr), row=1, col=1)
            fig.add_trace(go.Scatter(x=ts, y=cmd_log[idx],    name='[%d] Command #%d'             % (log_cntr,idx), marker_color=cstr), row=2, col=1)
            fig.add_trace(go.Scatter(x=ts, y=volt_log[idx],   name='[%d] Voltage #%d (V)'         % (log_cntr,idx), marker_color=cstr), row=3, col=1)
            fig.add_trace(go.Scatter(x=ts, y=curr_log[idx],   name='[%d] Current #%d (A)'         % (log_cntr,idx), marker_color=cstr), row=4, col=1)
            fig.add_trace(go.Scatter(x=ts, y=temp_log[idx],   name='[%d] Temperature #%d (deg C)' % (log_cntr,idx), marker_color=cstr), row=5, col=1)

log_path  = 'logs'
filenames = glob.glob(log_path+'/*_log*.txt')
filenames.sort(key=os.path.getmtime)

print(len(filenames))
print(filenames)

log_cntr = 0
for filename in filenames:
      process_file(filename, log_cntr)
      log_cntr += 1

fig.update_layout(title_text='Voxl ESC Test Results')
fig.update_xaxes(title_text="Time (s)",row=1, col=1)
fig.update_yaxes(title_text="Reported RPM",row=1, col=1)
fig.update_xaxes(title_text="Time (s)",row=2, col=1)
fig.update_yaxes(title_text="ESC Command (%)",row=2, col=1)
fig.update_xaxes(title_text="Time (s)",row=3, col=1)
fig.update_yaxes(title_text="Voltage (V)",row=3, col=1)
fig.update_xaxes(title_text="Time (s)",row=4, col=1)
fig.update_yaxes(title_text="Current (A)",row=4, col=1)
fig.update_xaxes(title_text="Time (s)",row=5, col=1)
fig.update_yaxes(title_text="Temperature (deg C)",row=5, col=1)

fig.update_xaxes(matches='x')
fig.show()

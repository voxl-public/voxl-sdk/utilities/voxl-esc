# When is ESC Calibration Required?
- when a new type of motor or new type of propeller is used
- when switching battery types (from 3S LiPo to 4S LiPo, etc)

# Hardware Setup
- Battery (check the voltage requirements for the specific ESC). Please note that testing with a power supply can damage the ESC, see [regenerative braking](doc/regenerative_braking.md)
- use appropriate wire gauge for the maximum current (usually 18 or 16 gauge is OK for small or medium sized slow flyer drones, 14 or 12 gauge for high current / racing applications) and try to minimize the length of the power cable
- identify the motor that will be used for tuning and attach the propeller to that motor. This calibration must be done with propeller attached!
- make sure the frame with the motor is firmly attached to some platform and cannot move when thrust is applied
- connect power connector from battery to ESC
- connect UART adapter to PC / Laptop
- apply power and make sure there are no unusual sounds or smell (besides the short beep ESC makes with motors when power is applied)

# Software Setup
- download voxl-esc tools and install any required prerequisites
 - see instructions at https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-esc/-/tree/master/voxl-esc-tools
- locate an existing tuning params file that most closely matches the motor that you have
- make a copy of the existing params file and update it if needed
    - specifically, make sure that ```num_cycles_per_rev``` is correct (usually 6 or 7 corresponding to number of pole pairs), otherwise RPM will not be calculated correctly
    - set ```vbat_nomival_mv``` to nominal battery voltage
    - leave other parameters the same, as they may not be known
    - upload the params file ```./voxl-esc-upload-params.py --params-file ../voxl-esc-params/<params_file>.xml```

# Detect ESC and Perform a Quick Spin Test
- detect the ESCs, make sure the software is the same version
 - ```./voxl-esc-scan.py```
- do a quick spin at lower power (10%)
 - ```./voxl-esc-spin.py --id 0 --power 10```
 - make sure the propeller is spinning in the correct direction (CW or CCW depending on propeller). Reverse one pair of motor wires to switch direction if needed
 - compare RPM to what is reported by tachometer
 - do not command very large power (open loop commands), since it will result in a very large step (less than 30)

## Calibrate ESC
- wear safety glasses
  - make sure the propeller is clear of any obstacles and there are no items that can become airborne due to high air flow
- calibrate the ESC's feed-forward curve
 - ```./voxl-esc-calibrate.py --id 0 --pwm-min 10 --pwm-max 95```
  - this will step the motor through a range of power from 10% to 95%
- calibration results plot will be saved to `calibration_results.html` and can be downloaded from VOXL2 to view on PC
  - if PC with a monitor is used (with direct usb to serial adapter connected to ESC), the plot will show up in a browser window

## Sample Calibration Results
- motor MN4006-23 380kV, 18N24P winding configuration (12 pole pairs = 24/2 )
- 15-inch propeller MS1503
- 6S battery voltage
- first plot (top left) is the commaded voltage vs RPM, that is the average voltage supplied to the motor in order to achieve each RPM. The curve has a nice smooth shape with slight quadratic (non-linear) upward trend
- plot on top right shows current draw vs time, it should be roughly quadratic because as the average voltage applied to the (loaded) motor increases, the current will increase quadratically.
- bottom right plot shows the rpm response vs time. The green samples are the samples that were used for calibration (as opposed to red samples showing all the data). This is showing that the samples were used were taken after the RPM has settled after a transition.
- plot of current and rpm vs time can be used to check if the ESC is behaving normally (plots look similar to this example) or there are some sudden jumps up or down
![esc calibration sample](doc/img/esc_calibration_mn4006_15in_6s.png)
- zoomed in
![esc calibration sample](doc/img/esc_calibration_mn4006_15in_6s_zoom.png)

## How to find minimum and maximum RPM
- minimum and maximum RPMs are a function of battery voltage (the higher the voltage, the higher the RPM for a given power %)
- it is suggested to measure those values either at nominal battery voltage (22.2-23.0V for 6S)
- a better result would be to measure `minimum RPM` at `maximum voltage` and `maximum RPM` at `minimum voltage`
   - Then you can always be sure the min and max rpm will be achieved regardless of battery charge level
- find minimum rpm:
   - `./voxl-esc-spin.py --id <motor id> --power 10`
- find maximum rpm (ramp up to avoid unnecessary jump and time out to avoid overheating)
  - `./voxl-esc-spin.py --id <motor id> --power 100 --ramp-time 3.0 --timeout 3.5`

## Update and Upload Calibration File
- update calibration parameters file printed by the calibration script
   - ```pwm_vs_rpm_curve_a0, pwm_vs_rpm_curve_a1, pwm_vs_rpm_curve_a2```
   - ```min_rpm``` and ```max_rpm```
- upload the calibration file to all ESCs. This will also reboot all the ESCs.
   - ```./voxl-esc-upload-params.py --params-file ../voxl-esc-params/<params_file>.xml```
- validate the esc parameters: ```./voxl-esc-verify-params.py```

## Test ESC using RPM commands
- choose RPM to test according to motor limits
- with proper tuning, the motor should track RPM very closely (typically within 100 RPM across the whole range)
- voltage compensation is automatic, so try adjusting voltage up and down 1-2 volts and RPM tracking should remain constant
```
./voxl-esc-spin.py --id 3 --rpm 4000 --ramp-time 2.0
...
[6.930] (3) RPM: 4023, PWR: 58, VOLTAGE: 23.933V, TEMPERATURE: 38.15C, CURRENT: 4.024A
[6.950] (3) RPM: 4000, PWR: 58, VOLTAGE: 23.929V, TEMPERATURE: 38.20C, CURRENT: 3.936A
[6.970] (3) RPM: 4014, PWR: 58, VOLTAGE: 23.935V, TEMPERATURE: 38.15C, CURRENT: 3.912A
[6.990] (3) RPM: 4000, PWR: 58, VOLTAGE: 23.929V, TEMPERATURE: 38.15C, CURRENT: 3.976A
[7.010] (3) RPM: 4000, PWR: 58, VOLTAGE: 23.940V, TEMPERATURE: 38.14C, CURRENT: 3.968A
[7.030] (3) RPM: 4011, PWR: 58, VOLTAGE: 23.929V, TEMPERATURE: 38.15C, CURRENT: 4.040A
...
```

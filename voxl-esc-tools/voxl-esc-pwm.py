#!/usr/bin/python3

from crc import crc16
import numpy as np
import serial
import time
import struct
import argparse
import string
import sys

from voxl_esc_setup_paths import *
voxl_esc_setup_paths()
from libesc import *
from esc_scanner import EscScanner

# PWM support on ModalAi ESCs:
# M0049, M0117, M0134, M0129 : IDs 0, 1, 2, 3
# M0138: IDs 0, 3 only
# All boards use pin A2 for PWM output


ESC_PACKET_HEADER                  = 0xAF
ESC_PACKET_TYPE_GPIO_CMD           = 15

parser = argparse.ArgumentParser(description='ESC PWM Test Script')
parser.add_argument('--device',         required=False, default=None)
parser.add_argument('--baud-rate',      required=False, default=None)
parser.add_argument('--id',             required=False,  default=0)
parser.add_argument('--pin',   type=str, required=False,  default='A2')
parser.add_argument('--val',   type=float,required=False,  default=1000)
parser.add_argument('--freq',  type=float,required=False,  default=200)
parser.add_argument('--timeout',  type=int,required=False,  default=1)
parser.add_argument('--sweep-step',  type=int,required=False,  default=10)
parser.add_argument('--sweep-count',  type=int,required=False,  default=0)
parser.add_argument('--sweep-delay',  type=float,required=False,  default=0.01)

args = parser.parse_args()

devpath  = args.device
baudrate = args.baud_rate
esc_id   = args.id
pin      = args.pin
val      = args.val
freq     = args.freq
timeout  = args.timeout

sweep_step  = args.sweep_step
sweep_count = args.sweep_count
sweep_delay = args.sweep_delay

if val < 0 or val > 2201:
    print('ERROR: val argument must be between 0 and 2201')
    sys.exit(1)

if val == 2201:
    print('\nWARNING: value 2201 will set the pwm output to constant high\n')

if freq != 50 and freq != 200 and freq != 400:
    print('\nERROR: pwm frequency must be either 50, 200, or 400 (Hz)')
    sys.exit(1)

def wrap_data(packet_type, data):
    data_length = len(data)

    packet_length = data_length + 5 # add bytes for header(1), length(1), type(1), crc(2) 

    packet = [ESC_PACKET_HEADER,
              packet_length,
              packet_type]

    if data_length > 0:
        packet.extend(data)

    crc_val = crc16(packet[1:]) #do not include header byte

    packet = np.uint8(packet)
    packet = np.append(packet, np.frombuffer(crc_val, dtype=np.uint8))

    return packet


class SerialPort:
    dev       = None
    dev_path  = ''
    baud_rate = 115200

    def __init__(self, device_path, baud_rate):
        self.dev_path  = device_path
        self.baud_rate = baud_rate

    def open(self):
        self.dev              = serial.Serial()
        self.dev.port         = self.dev_path
        self.dev.baudrate     = self.baud_rate
        self.dev.parity       = serial.PARITY_NONE
        self.dev.bytesize     = serial.EIGHTBITS
        self.dev.stopbits     = serial.STOPBITS_ONE
        self.dev.timeout      = 0.025
        self.dev.writeTimeout = 0.025

        self.dev.open()
        self.dev.flush()

    def close(self):
        self.dev.flush()
        self.dev.close()

    def flush(self):
        self.dev.flushInput()
        self.dev.flush()

    def write(self, data):
        self.dev.write(data)

    def read(self):
        data_array=''
        if self.dev.inWaiting() > 0:
            data_array = self.dev.read(self.dev.inWaiting())

        return data_array


# quick scan for ESCs to detect the port
scanner = EscScanner()
(devpath, baudrate) = scanner.scan(devpath, baudrate)

if devpath is not None and baudrate is not None:
    print('INFO: ESC(s) detected on port: ' + devpath + ', baud rate: ' + str(baudrate))
else:
    print('ERROR: No ESC(s) detected, exiting.')
    sys.exit(1)


if 'slpi' in devpath:
    from voxl_serial import VoxlSerialPort
    port = VoxlSerialPort()
    port.port = devpath
    port.baudrate = baudrate
    port.open()
else:
    port = SerialPort(devpath,baudrate);
    port.open()

#note that GPIO-like functionality can be achieved by setting pulse_us to either 0 (low) or 2201 (high)

pulse_us   = val;
pulse_mode = 2 #50hz

if freq == 50:
    pulse_mode = 2
elif freq == 200:
    pulse_mode = 4
elif freq == 400:
    pulse_mode = 6
else:
    print('ERROR: unknown pwm pulse mode')
    sys.exit(1)


#parse the MCU port and pin value from --pin argument
try:
    pin_port     = pin[0].upper()
    pin_id       = int(pin[1:])
    pin_port_dec = ord(pin_port)-ord('A')

    if pin_port_dec < 0 or pin_port_dec > 5:
        print('ERROR: pwm port should be one of A, B, C, D, E, or F')
        print('provided pin value: %s' % pin)
        sys.exit(1)

    if pin_id <0 or pin_id > 15:
        print('ERROR: pwm pin should be between 0 and 15 (inclusive)')
        print('provided pin value: %d (%s)' % (pin_id,pin))
        sys.exit(1)

    #print('pin port %s, pin id %d' % (pin_port, pin_id))
except Exception as e:
    print("\nERROR parsing port pin number from string \"%s\": " % pin, e)
    print("--pin argument should have values like A2 or B8")
    sys.exit(1)

pin_val = pin_port_dec << 4 | pin_id

if timeout == 0:
    pulse_mode += 1 #pulse modes 3,5,7 for disabled timeout

for i in range(sweep_count+1):
    pulse_cmd   = int((pulse_us+i*sweep_step) * 20) #pulse is sent in 0.05us resolution

    req_data    = np.zeros(5,dtype=np.uint8)
    req_data[0] = esc_id  # esc id
    #req_data[1] = 2       # pwm pin : 00000010 : pin A2 : PWM output is currently only supported on pin A2
    req_data[1] = pin_val
    req_data[2] = pulse_mode       # 2,3: 50hz pwm, 4,5: 200hz pwm, 6,7: 400hz pwm
    req_data[3] = pulse_cmd      & 0xFF  # cmd LSB
    req_data[4] = (pulse_cmd>>8) & 0xFF  # cmd MSB

    packet = wrap_data(ESC_PACKET_TYPE_GPIO_CMD,req_data)
    #print('sending packet:', packet)

    port.write(packet);
    time.sleep(sweep_delay);

#time.sleep(0.1);  #sleep to allow the command to go out before port is closed
